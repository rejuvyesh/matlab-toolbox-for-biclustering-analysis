function plaidtest(data_type,nbiclust,numData)

    
for i=1:numData
    cd ([data_type]);
    allFile = load([num2str(i) '.mat']);
    data = allFile.data;
    expected = allFile.expected;
    
    plaidres = biclust(data, 'plaid', 'iter_startup', 100, 'iter_layer', 200, 'verbose', false);
    
    if plaidres.ClusterNo>0
        set(gcf,'visible','off')
        heatmap(data, plaidres, 1);
        print(gcf, '-dpng', [num2str(i) 'plaid.png']);
        close;
        [plaidrel, plaidrec] = rrscores(expected, plaidres, data);
        [plaidjacc1, plaidjacc2]    = rrscores(expected, plaidres, data, @jaccard, @jaccard, ...
                                  @max);
        [plaidfmeasure1, plaidfmeasure2] = rrscores(expected, plaidres, data, @f_measure, @f_measure, @max);
        plaidmiscore      = NormalizedMutualInformation(data, expected, ...
                                                        plaidres);
    else
        plaidrel=0; plaidrec=0; plaidjacc1=0; plaidfmeasure1=0; plaidmiscore=0;
    end

    save(['plaid' num2str(i) '.mat']);
    cd ..
end
end