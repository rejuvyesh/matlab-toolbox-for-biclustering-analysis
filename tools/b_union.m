function bicluster = b_union(bic1, bic2)
    bicluster.rows = union(bic1.rows, bic2.rows);
    bicluster.cols = union(bic1.rows, bic2.cols);
end 
