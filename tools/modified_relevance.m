function rel = modified_relevance(expected, found)
% Calculates proportion of expected retrieved in found
%
% Author: Jayesh Kumar Gupta, 2013.
%
% Contact: Jayesh Kumar Gupta http://home.iitk.ac.in/~jayeshkg
%          Indian Institute of Technology, Kanpur, India

    if is_area_nonzero(found)
        rel = intersection_b_area(expected, found) / b_area(found);
    else
        rel = 0;
    end
    
end
