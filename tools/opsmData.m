function [data, expectedBicluster] = opsmData(rowxNum, numxCol, shuffleClust)

if nargin==2
    shuffleClust = 0;
end
data = zeros(size(rowxNum,1),size(numxCol,2));
expectedBicluster = make_expected_biclusters(rowxNum, numxCol);

for num=1:size(rowxNum,2)
    rowClust = find(rowxNum(:,num));
    colClust = find(numxCol(num,:))';
    if shuffleClust==1
        order = randperm(length(colClust));
    else
        order = sort(randperm(length(colClust)));
    end
    for i=rowClust(1):rowClust(end)
        array = sort(randn(1,length(colClust)));
        for j=1:length(colClust)
            data(i,colClust(j)) = array(order(j));
        end
    end
end
% filling the background
for i = 1:size(data,1)
    for j = find(data(i,:)==0)
        data(i,j) = randn(1);
    end
end
end