function [shuffled_data, shuffled_biclusters] = shuffle(data, expected, new_rows, new_cols)
% Shuffle the dataset while preserving biclusters
    if nargin<3
        new_rows = [];
        new_cols = [];
    end
    if nargin<4
        new_cols = [];
    end

    [nrows, ncols] = size(data);
    if isempty(new_rows)
        new_rows = randperm(nrows);
    end
    if isempty(new_cols)
        new_cols = randperm(ncols);
    end

    shuffled_data = data(new_rows, new_cols);
    shuffled_biclusters.RowxNum = expected.RowxNum(new_rows,:);
    shuffled_biclusters.NumxCol = expected.NumxCol(:,new_cols);
    shuffled_biclusters.ClusterNo = expected.ClusterNo;
    for i=1:shuffled_biclusters.ClusterNo
        rows = find(shuffled_biclusters.RowxNum(:,i)>0);
        cols = find(shuffled_biclusters.NumxCol(i,:)>0);
        shuffled_biclusters.Clust(i) = struct('rows', rows, 'cols', cols);
    end 
end
