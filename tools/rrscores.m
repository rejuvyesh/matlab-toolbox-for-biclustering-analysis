function [relev, reco] = rrscores(expected, found, data, rec_func, ...
                                          rel_func, comp)
    % Finds recovery and relevance scores of the bicluster result. The overall
    % score is computed as the mean of the best scores for each bicluster
    %
    % Author: Jayesh Kumar Gupta, 2013.
    %
    % Contact: Jayesh Kumar Gupta http://home.iitk.ac.in/~jayeshkg
    %          Indian Institute of Technology, Kanpur, India

    % recovery  = mean(arrayfun(rec_func, expected.Clust, found.Clust)));
% relevance = mean(arrayfun(rel_func, expected.Clust, found.Clust));

if nargin<4
    rec_func = @recovery;
    rel_func = @modified_relevance;
    comp     = @max;
end 
max_rec_a = [];
max_rel_a = [];
for i = 1:expected.ClusterNo
    max_rec = -1;
    for j = 1:found.ClusterNo
        max_rec = comp(max_rec, rec_func(expected.Clust(i), ...
                                        found.Clust(j)));
    end
    max_rec_a(end+1) = max_rec;
end

for i = 1:found.ClusterNo
    max_rel = -1;
    for j = 1:expected.ClusterNo
        if isequal(func2str(rel_func),'relevance')  
          max_rel = comp(max_rel, rel_func(expected.Clust(j), found.Clust(i), numel(data)));
        else 
          max_rel = comp(max_rel, rel_func(expected.Clust(j), found.Clust(i)));
        end
        
    end
    max_rel_a(end+1) = max_rel;
end

reco  = mean2(max_rec_a);
relev = mean2(max_rel_a);
end
