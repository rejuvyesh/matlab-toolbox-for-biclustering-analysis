function result = make_expected_biclusters(rowxnum, numxcol)
% Construct the expected biclusters in Biclustresult format

    nclust = size(rowxnum,2);
    assert(nclust == size(numxcol, 1));

    cluster = struct('rows', [], 'cols',[]);
    for i=1:nclust
        rows = find(rowxnum(:,i)>0);
        cols = find(numxcol(i,:)>0);
        cluster(i) = struct('rows', rows, 'cols', cols);
    end

    result = struct('RowxNum', rowxnum, 'NumxCol', numxcol, 'ClusterNo', nclust, ...
                    'Clust', cluster);
end