function [data, expected] = isa_biclust(rowxnum, numxcol, noise, bi_signal, bi_noise)
% Create isa type biclusters
%
% Author: Jayesh Kumar Gupta, 2013.
%
% Contact: Jayesh Kumar Gupta http://home.iitk.ac.in/~jayeshkg
%          Indian Institute of Technology, Kanpur, India
    if nargin < 3
        noise = 0.1;
        bi_signal = ones(size(numxcol,1), 1);
        bi_noise  = zeros(size(numxcol,1), 1);
    end
    for i=1:size(numxcol,1)
        rowxnum(find(rowxnum(:,i)>0),i) = sqrt(bi_signal(i));
        numxcol(i,find(numxcol(i,:)>0)) = sqrt(bi_signal(i));
    end
    data = rowxnum * numxcol;
    data = data + normrnd(0, noise, size(data));
    for i=1:size(numxcol,1)
        rowxnum(find(rowxnum(:,i)>0),i) = 1;
        numxcol(i,find(numxcol(i,:)>0)) = 1;
    end
    
    % Add specific noise to bicluster
    % for i = bi_noise
        
    % end
    expected = make_expected_biclusters(rowxnum, numxcol);
    
end
