function  check = is_area_nonzero(bic)
    if b_area(bic) == 0
        check = 0;
        nrows = size(bic.rows,1);
        ncols = size(bic.cols,2);
        msg   = ['bicluster is not properly defined: ' num2str(nrows) ' rows and ' ...
                 num2str(ncols)  ' columns'];
        warning(msg);
    else
        check = 1;
    end
end
