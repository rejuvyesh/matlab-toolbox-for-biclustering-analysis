#!/bin/sh
#
# File: win.sh
#
# Created: Wednesday, December 11 2013 by rejuvyesh <mail@rejuvyesh.com>
# License: GNU GPL 3 <http://www.gnu.org/copyleft/gpl.html>
#
TMW_ROOT="$MATLAB"
MLIBS="-L$TMW_ROOT/bin/$Arch -lmx -lmex -lmat"
RPATH="-Wl,-rpath-link,$TMW_ROOT/bin/$Arch"
# StorageVersion: 1.0
# CkeyName: GNU C
# CkeyManufacturer: GNU
# CkeyLanguage: C
# CkeyVersion:
CC='x86_64-w64-mingw32-gcc'
CFLAGS='-std=c99 -D_GNU_SOURCE'
CFLAGS="$CFLAGS  -fexceptions"
CFLAGS="$CFLAGS -fPIC -fno-omit-frame-pointer -pthread -I/usr/x86_64-w64-mingw32/include"
CLIBS="$RPATH $MLIBS  -L/usr/x86_64-w64-mingw32/lib -L /mnt/Windows/Program\ Files/MATLAB/R2012a/bin/win64/ -lm -lgsl -lgslcblas"
COPTIMFLAGS='-O -DNDEBUG'
CDEBUGFLAGS='-g'
CLIBS="$CLIBS -lstdc++"
#
# C++keyName: GNU C++
# C++keyManufacturer: GNU
# C++keyLanguage: C++
# C++keyVersion: 
CXX='x86_64-w64-mingw32-g++'
CXXFLAGS='-ansi -D_GNU_SOURCE'
CXXFLAGS="$CXXFLAGS -fPIC -fno-omit-frame-pointer -pthread"
CXXLIBS="$RPATH $MLIBS -lm"
CXXOPTIMFLAGS='-O -DNDEBUG'
CXXDEBUGFLAGS='-g'
#
# FortrankeyName: gfortran
# FortrankeyManufacturer: GNU
# FortrankeyLanguage: Fortran
# FortrankeyVersion: 
#
FC='x86_64-w64-mingw32-gfortran'
FFLAGS='-fexceptions -fbackslash'
FFLAGS="$FFLAGS -fPIC -fno-omit-frame-pointer"
FLIBS="$RPATH $MLIBS -lm"
FOPTIMFLAGS='-O'
FDEBUGFLAGS='-g'
#
LD="$COMPILER"
LDEXTENSION='.mexw64'
LDFLAGS="-pthread -shared -Wl,--version-script,$TMW_ROOT/extern/lib/$Arch/$MAPFILE -Wl,--no-undefined"
LDOPTIMFLAGS='-O'
LDDEBUGFLAGS='-g'
#
POSTLINK_CMDS=':'
