function jacscore = jaccard(expected, found)
% Jaccard coefficient of bicluster area
% $\frac{|e \cap f|}{|e \cup f|}$
%
% Author: Jayesh Kumar Gupta, 2013.
%
% Contact: Jayesh Kumar Gupta http://home.iitk.ac.in/~jayeshkg
%          Indian Institute of Technology, Kanpur, India

    jacscore = intersection_b_area(expected, found)/union_b_area(expected, found);
end