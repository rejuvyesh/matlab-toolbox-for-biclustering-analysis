function BBCtest(data_type,nbiclust,numData)

for i=1:numData
    cd (['C:\clustering project\mtbc\tools\' data_type]);
    allFile = load([num2str(i) '.mat']);
    data = allFile.data;
    expected = allFile.expected;
    if nbiclust==1
        BBCres = biclust(data, 'bayes', 'numclusters', (nbiclust+1), 'normchoice', 3);
    else
        BBCres = biclust(data, 'bayes', 'numclusters', nbiclust, 'normchoice', 3);
    end
    if BBCres.ClusterNo>0
        set(gcf,'visible','off')
        heatmap(data, BBCres, 1);
        cd ..
        print(gcf, '-dpng', [data_type '/' num2str(i) 'BBC.png']);
        close;
        [BBCrel, BBCrec] = rrscores(expected, BBCres, data);
        [BBCjacc1, BBCjacc2]    = rrscores(expected, BBCres, data, @jaccard, @jaccard, ...
                                  @max);
        [BBCfmeasure1, BBCfmeasure2] = rrscores(expected, BBCres, data, @f_measure, @f_measure, @max);
        BBCmiscore      = NormalizedMutualInformation(data, expected, BBCres);
    end
    save([data_type '/BBC' num2str(i) '.mat']);
end
end