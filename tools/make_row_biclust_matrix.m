function rowxnum = make_row_biclust_matrix(nrows, nclusts, nclust_rows, ...
                                           noverlap_rows)
% Make RowxNum matrix for biclust
    % Hack for default nclust_rows
    if nclust_rows==0
        nclust_rows = floor(nrows/nclusts);
        
    end
    if nclust_rows == nrows
        nclust_rows = floor(nrows/2);
    end
    % input check
    if nclust_rows+(nclust_rows - noverlap_rows) * (nclusts -1) > nrows
        error('biclusters are too large to fit in the dataset');
    end
    rowxnum = zeros(nrows, nclusts);
    start=1;
    for i = 1:nclusts
        stop = start + nclust_rows - 1;
        rowxnum(start:stop, i) = 1;
        start = (nclust_rows * i) - (noverlap_rows * i) + 1;
    end
end