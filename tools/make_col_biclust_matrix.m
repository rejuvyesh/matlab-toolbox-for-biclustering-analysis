function numxcol = make_col_biclust_matrix(ncols, nclusts, nclust_cols, ...
                                               noverlap_cols)
    % make NumxCol matric for biclust
    numxcol = make_row_biclust_matrix(ncols, nclusts, nclust_cols, ...
                                      noverlap_cols);
    numxcol = numxcol';
end