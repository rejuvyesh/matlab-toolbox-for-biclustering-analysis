function [data, expectedBicluster] = coherentSignData(rowxNum,numxCol)

expectedBicluster = make_expected_biclusters(rowxNum,numxCol);
data = zeros(size(rowxNum,1),size(numxCol,2));

for num=1:size(rowxNum,2)
    rowClust = find(rowxNum(:,num));
    rowPerm = randi([0 1],1,length(rowClust));
    rowPerm(rowPerm==0) = -1;
    colClust = find(numxCol(num,:))';
    colPerm = randi([0 1],1,length(colClust));
    colPerm(colPerm==0) = -1;
    for i=1:length(rowClust)
        array = colPerm.*(rowPerm(1,i)*abs(5*(1+randn(1,length(colClust)))));
        for j=1:length(colClust)
            data(rowClust(i),colClust(j)) = array(j);
        end
    end
end
% filling the background
for i = 1:size(data,1)
    for j = find(data(i,:)==0)
        data(i,j) = randn(1);
    end
end

end