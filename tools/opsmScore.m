function opsmScore(rowxNumFile,numxColFile,expectedBicluster,data)
rowxNum = load(rowxNumFile);
numxCol = load(numxColFile);
opsmRes = make_expected_biclusters(rowxNum,numxCol);

if opsmRes.ClusterNo>0
        heatmap(data, opsmRes, 1);
        print(gcf, '-dpng', [rowxNumFile 'opsm.png']);
        close;
        [opsmrel, opsmrec] = rrscores(expectedBicluster, opsmRes, data);
        [opsmjacc1, opsmjacc2]        = rrscores(expectedBicluster, opsmRes, data, @jaccard, ...
                                          @jaccard, @max);
        [opsmfmeasure1, opsmfmeasure2] = rrscores(expectedBicluster, opsmRes, data, @f_measure, @f_measure, @max);
        opsmmiscore      = NormalizedMutualInformation(data, expectedBicluster, opsmRes);
    end
    
end