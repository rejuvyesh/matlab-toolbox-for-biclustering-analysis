function iar = intersection_b_area(bic1, bic2)
    iar = b_area(intersection(bic1, bic2));
end
