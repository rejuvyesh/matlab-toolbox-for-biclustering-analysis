function fmeas = f_measure(expected, found, beta, modified)
% Calcultes the f_measure score using relevance and recovery scores and
% thaking their harmonic mean
%
% Author: Jayesh Kumar Gupta, 2013.
%
% Contact: Jayesh Kumar Gupta http://home.iitk.ac.in/~jayeshkg
%          Indian Institute of Technology, Kanpur, India
    if nargin<3
        beta = 1;
        modified = true;
    end
    if modified
        prec = modified_relevance(expected, found);
    else
        prec = relevance(expected, found);
    end
    rec = recovery(expected, found);
    if prec == 0 && rec == 0
        fmeas = 0;
        return
    end
    fmeas = (1+beta^2) * (prec * rec) / (beta^2 * prec + rec);
end 