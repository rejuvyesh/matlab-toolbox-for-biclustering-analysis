function bicluster = intersection(bic1, bic2)
    bicluster.rows = intersect(bic1.rows, bic2.rows);
    bicluster.cols = intersect(bic1.cols, bic2.cols)';
end
