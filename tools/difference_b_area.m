function dar = difference_b_area(bic1, bic2)
    dar = b_area(bic1) - intersection_b_area(bic1, bic2)
end 
