function floctest(data_type,nbiclust,numData)

    
for i=1:numData
    cd ([data_type]);
    allFile = load([num2str(i) '.mat']);
    data = allFile.data;
    expected = allFile.expected;
    
    flocres = biclust(data, 'floc');
    
    if flocres.ClusterNo>0
        set(gcf,'visible','off')
        heatmap(data, flocres, 1);

        print(gcf, '-dpng', [num2str(i) 'floc.png']);
        close;
        [flocrel, flocrec] = rrscores(expected, flocres, data);
        [flocjacc1, flocjacc2]    = rrscores(expected, flocres, data, @jaccard, @jaccard, ...
                                  @max);
        [flocfmeasure1, flocfmeasure2] = rrscores(expected, flocres, data, @f_measure, @f_measure, @max);
        flocmiscore      = NormalizedMutualInformation(data, expected, ...
                                                        flocres);
    else
        flocrel=0; flocrec=0; flocjacc1=0; flocfmeasure1=0; flocmiscore=0;
    end

    save(['floc' num2str(i) '.mat']);
    cd ..
end
end