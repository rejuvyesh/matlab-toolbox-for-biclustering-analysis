function NMI = NormalizedMutualInformation(data, expectedBic, obtainedBic)

N=size(data,1)*size(data,2);
expNumClust = expectedBic.ClusterNo;
obNumClust = obtainedBic.ClusterNo;
MI = 0;
obEntropy = zeros(1,obNumClust);
for i=1:expNumClust
    expCols = expectedBic.Clust(i).cols;
    expRows = expectedBic.Clust(i).rows;
    expNum = length(expCols)*length(expRows);    
    expEntropy(i) = -expNum*log10(expNum/N)/N;
end

for i=1:obNumClust
    obCols = obtainedBic.Clust(i).cols;
    obRows = obtainedBic.Clust(i).rows;
    obNum = length(obCols)*length(obRows);
    if obNum>0
        obEntropy(i) = -obNum*log10(obNum/N)/N;
    end
    for j=1:expNumClust
        expCols = expectedBic.Clust(j).cols;
        expRows = expectedBic.Clust(j).rows;
        expNum = length(expCols)*length(expRows);
        expEntropy(j) = -expNum*log10(expNum/N)/N;
        common = length(intersect(expCols,obCols))*...
                    length(intersect(expRows,obRows));
        if common>0
            MI = MI + common*log10(N*common/(expNum*obNum))/N;
        end
    end
end
if MI<0
    NMI = 0;
else
    NMI = 2*MI/(sum(obEntropy)+sum(expEntropy));
end
end