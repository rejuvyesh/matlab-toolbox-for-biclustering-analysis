function prelscore = row_jaccard(expected, found)
% Calculates Jaccard coefficient of rows only (as in Prelic)
%
% Author: Jayesh Kumar Gupta, 2013.
%
% Contact: Jayesh Kumar Gupta http://home.iitk.ac.in/~jayeshkg
%          Indian Institute of Technology, Kanpur, India

    intersect = intersection(expected, found);
    union    =  b_union(expected, found);
    if length(union.rows) == 0
        prelscore = 0;
        return;
    end
    prelscore = length(intersect.rows) / length(union.rows);
end 
