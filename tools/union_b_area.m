function uar = union_b_area(bic1, bic2)
    uar = b_area(bic1) + b_area(bic2) - intersection_b_area(bic1, bic2);
end
