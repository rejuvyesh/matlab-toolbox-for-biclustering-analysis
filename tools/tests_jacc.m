function tests_jacc(matrixsize, nbiclust, data_type)
% Generate biclusters and run tests

    rowxNum = make_row_biclust_matrix(matrixsize.rows, nbiclust, 0, 0);
    numxCol = make_col_biclust_matrix(matrixsize.cols, nbiclust, 0, 0);
    % Generate dataset
    switch data_type
      case 'constant_up'
        [data, expected] = constant_biclust(rowxNum, numxCol, 5);
      case 'shift-scale'
        shift = 1;
        scale = 1;
        [data, expected] = shift_scale_data(rowxNum, numxCol, shift, scale);
      case 'shift'
        shift = 1;
        scale = 0;
        [data, expected] = shift_scale_data(rowxNum, numxCol, shift, scale);
      case 'scale'
        shift = 0;
        scale = 1;
        [data, expected] = shift_scale_data(rowxNum, numxCol, shift, scale);
      case 'plaid'
        [data, expected] = plaid_dataset(matrixsize.rows, matrixsize.cols, nbiclust, 0);
      case 'plaid_noise'
        [data, expected] = plaid_dataset(matrixsize.rows, matrixsize.cols, nbiclust, 1);
      otherwise
        error('no data generated');
    end
    
    % Save heatmap of generated data
    heatmap(data);
    print(gcf,'-dpng',strcat(data_type,'_data.png'));

    % Run different algorithms
    ccres = biclust(data, 'cc', 'alpha', 1.5, 'delta', 1, 'numbiclust', nbiclust);
    if ccres.ClusterNo>0
        heatmap(data, ccres, 1);
        print(gcf, '-dpng', strcat(datestr(now,30), data_type,'cc.png'));
        close;
        [ccrel, ccrec] = rrscores(expected, ccres, data, @jaccard, @jaccard, ...
                                    @max);
    end
    plaidres = biclust(data, 'plaid');
    if plaidres.ClusterNo>0
        heatmap(data, plaidres, 1);
        print(gcf, '-dpng', strcat(datestr(now,30), data_type,'plaid.png'));
        close;
        [plaidrel, plaidrec] = rrscores(expected, plaidres, data, @jaccard, @jaccard, ...
                                    @max);
    end
    isares = biclust(data, 'isa');
    if isares.ClusterNo>0
        heatmap(data, isares, 1);
        print(gcf, '-dpng', strcat(datestr(now,30), data_type,'isa.png'));
        close;
        [isarel, isarec] = rrscores(expected, isares, data, @jaccard, @jaccard, ...
                                    @max);
    end
    bimaxres = biclust(data, 'bimax', 'numbiclust', nbiclust);
    if bimaxres.ClusterNo>0
        heatmap(data, bimaxres, 1);
        print(gcf, '-dpng', strcat(datestr(now,30), data_type,'bimax.png'));
        close;
        [bimaxrel, bimaxrec] = rrscores(expected, bimaxres, data, @jaccard, @jaccard, ...
                                    @max);
    end
    kspectralres = biclust(data, 'kSpectral');
    if kspectralres.ClusterNo>0
        heatmap(data, kspectralres, 1);
        print(gcf, '-dpng', strcat(datestr(now,30), data_type,'kspectral.png'));
        close;
        [kspectralrel, kspectralrec] = rrscores(expected, kspectralres, data, @jaccard, @jaccard, ...
                                    @max);
    end
    flocres = biclust(data, 'floc');
    if flocres.ClusterNo>0
        heatmap(data, flocres, 1);
        print(gcf, '-dpng', strcat(datestr(now,30), data_type,'floc.png'));
        close;
        [flocrel, flocrec] = rrscores(expected, flocres, data, @jaccard, @jaccard, ...
                                    @max);
    end
    xmotifres = biclust(data, 'xmotif', 'number', nbiclust);
    if xmotifres.ClusterNo>0
        heatmap(data, xmotifres, 1);
        print(gcf, '-dpng', strcat(datestr(now,30), data_type,'xmotif.png'));
        close;
        [xmotifrel, xmotifrec] = rrscores(expected, xmotifres, data, @jaccard, @jaccard, ...
                                    @max);
    end
    bsgpres = biclust(data, 'bsgp', 'n', nbiclust, 'display', 1);
    print(gcf, '-dpng', strcat(datestr(now,30), data_type,'bsgp.png')); % save spectral
    close;
    [bsgprel, bsgprec] = rrscores(expected, bsgpres, data, @jaccard, @jaccard, ...
                                    @max);
    lasres = biclust(data, 'las', 'numbc', nbiclust);
    if lasres.ClusterNo>0
        heatmap(data, lasres, 1);
        print(gcf, '-dpng', strcat(datestr(now,30), data_type,'las.png'));
        [lasrel, lasrec] = rrscores(expected, lasres, data, @jaccard, @jaccard, ...
                                    @max);
    end
    itlres = biclust(data, 'itl', 'numclust', nbiclust, 'display', 1);
    print(gcf, '-dpng', strcat(datestr(now,30), data_type, 'itl.png'));
    close;
    [itlprel, itlprec] = rrscores(expected, itlres, data, @jaccard, @jaccard, ...
                                    @max);
    save(strcat(datestr(now,30), data_type, '.mat'))
end

function [data, expectedBicluster] = constant_biclust(rowxNum, numxCol, biclusterValue)
% generate constant biclusters
if nargin==2
    biclusterValue = 0;
end

expectedBicluster = make_expected_biclusters(rowxNum, numxCol);
data = randn(size(rowxNum,1), size(numxCol,2));
for num=1:size(rowxNum,2)
    rowClust = find(rowxNum(:,num));
    colClust = find(numxCol(num,:))';
    matClust = rowxNum*numxCol;
    for i = rowClust(1):rowClust(end)
        for j = colClust(1):colClust(end)
            data(i,j) = matClust(i,j)*biclusterValue;
        end
    end
end
end

function [data, expectedBicluster] = shift_scale_data(rowxNum, numxCol, shift, scale)

expectedBicluster = make_expected_biclusters(rowxNum, numxCol);
initialMat = zeros(size(rowxNum,1),size(numxCol,2));
data = initialMat;
numColClust = sum(numxCol,2);
% filling the bicluster values
for num=1:size(rowxNum,2)
    matClust = initialMat;
    parent = randn(1,numColClust(num));
    rowClust = find(rowxNum(:,num));
    colClust = find(numxCol(num,:))';
    matClust(rowClust(1),colClust) = parent;
    if shift==1
        for i = rowClust(2):rowClust(end)
            shiftParameter = randn(1);
            for j = colClust(2):colClust(end)
                matClust(i,j) = matClust(rowClust(1),j) + shiftParameter;
            end
        end
    end
    
    if scale==1
        for i = rowClust(2):rowClust(end)
            scaleParameter = randn(1);
            for j = colClust(2):colClust(end)
                matClust(i,j) = scaleParameter*matClust(rowClust(1),j);
            end
        end
    end
    data = data + matClust;
end
% filling the background
for i = 1:size(data,1)
    for j = find(data(i,:)==0)
        data(i,j) = randn(1);
    end
end
end

function [matrix, expectedBicluster] = plaid_dataset(dataMatrixRowSize, dataMatrixColumnSize, numBicluster, noise)

    rowIndicator = randi([0 1], dataMatrixRowSize, numBicluster);
    columnIndicator = randi([0 1], dataMatrixColumnSize, numBicluster);
    expectedBicluster = make_expected_biclusters(rowIndicator, columnIndicator');
    backgroundEffect = randn(1);
    clusterEffect = randn(numBicluster,1);
    rowEffect = randn(dataMatrixRowSize, numBicluster);
    columnEffect = randn(dataMatrixColumnSize, numBicluster);
    if noise==1
        noise = randn(dataMatrixRowSize, dataMatrixColumnSize);
    else
        noise = zeros(dataMatrixRowSize, dataMatrixColumnSize);
    end


    for i=1:dataMatrixRowSize
        for j=1:dataMatrixColumnSize
            sum = 0;
            for k=1:numBicluster
                sum = sum + (clusterEffect(k,1) + rowEffect(i,k) + columnEffect(j,k))*rowIndicator(i,k)*columnIndicator(j,k);
            end
            matrix(i,j) = backgroundEffect + sum + noise(i,j); 
        end
    end
end
