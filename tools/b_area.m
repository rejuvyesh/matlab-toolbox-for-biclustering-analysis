function ar = b_area(bicluster)
ar = numel(bicluster.rows) * numel(bicluster.cols);
end 
