function data = shiftScaleData(rowxNum, numxCol, shift, scale)

initialMat = zeros(size(rowxNum,1),size(numxCol,2));
data = initialMat;
numColClust = sum(numxCol,2);

for num=1:size(rowxNum,2)
    matClust = initialMat;
    parent = randn(1,numColClust(num));
    rowClust = find(rowxNum(:,num));
    colClust = find(numxCol(num,:))';
    matClust(rowClust(1),colClust) = parent;
    if shift==1
        for i = rowClust(2):rowClust(end)
            shiftParameter = randn(1);
            for j = colClust(2):colClust(end)
                matClust(i,j) = matClust(rowClust(1),j) + shiftParameter;
            end
        end
    end
    
    if scale==1
        for i = rowClust(2):rowClust(end)
            scaleParameter = randn(1);
            for j = colClust(2):colClust(end)
                matClust(i,j) = scaleParameter*matClust(rowClust(1),j);
            end
        end
    end
    data = data + matClust;
end

for i = 1:size(data,1)
    for j = find(data(i,:)==0)
        data(i,j) = randn(1);
    end
end
end