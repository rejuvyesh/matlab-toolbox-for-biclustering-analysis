function matrix=replaceMissingVal(num,char,method)


for i=1:size(char,1)
    if strcmp(method,'rowAvg')
        val = strcmp(char(i,:),'');
        MissingVal = sum(num(i,val))/length(val);
    end
    for j=1:size(char,2)
        if strcmp(char(i,j),'null')
            if strcmp(method,'zero')
                num(i,j)=0;
            else if strcmp(method,'rowAvg')
                    num(i,j) = MissingVal;
                end
            end
        end
    end
end
matrix=num;
end