function tests(matrixsize, nbiclust, data_type, count)
% Generate biclusters and run tests

    rowxNum = make_row_biclust_matrix(matrixsize.rows, nbiclust, 50, 0);
    numxCol = make_col_biclust_matrix(matrixsize.cols, nbiclust, 50, 0);
    % Generate dataset
    switch data_type
      case 'constant_up'
        [data, expected] = constant_biclust(rowxNum, numxCol, [7 6 5]);
      case 'shift_scale'
        shift = 1;
        scale = 1;
        [data, expected] = shift_scale_data(rowxNum, numxCol, shift, scale);
      case 'shift'
        shift = 1;
        scale = 0;
        [data, expected] = shift_scale_data(rowxNum, numxCol, shift, scale);
      case 'scale'
        shift = 0;
        scale = 1;
        [data, expected] = shift_scale_data(rowxNum, numxCol, shift, scale);
      case 'plaid'
        [data, expected] = plaid_dataset(rowxNum, numxCol, 0);
      case 'plaid_noise'
        [data, expected] = plaid_dataset(rowxNum, numxCol, 1);
      case 'opsm'
        [data, expected]  = opsmData(rowxNum, numxCol);
      case 'coherentSignData'
        [data, expected] = coherentSignData(rowxNum,numxCol);    
      otherwise
        error('no data generated');
    end
    
    % Write data to csv
    csvwrite([data_type '/' num2str(count) '.csv'], data);
    % Save heatmap of generated data
    set(gcf,'visible','off')
    heatmap(data);
    close;
    print(gcf,'-dpng',[data_type '/' num2str(count) '_data.png']);
    
    % Run different algorithms
    ccres = biclust(data, 'cc', 'alpha', 1.5, 'delta', 0.1, 'numbiclust', nbiclust);
    if ccres.ClusterNo>0
        set(gcf,'visible','off')
        heatmap(data, ccres, 1);
        print(gcf, '-dpng', [data_type '/' num2str(count) 'cc.png']);
        close;
        [ccrel, ccrec] = rrscores(expected, ccres, data);
        [ccjacc1, ccjacc2]    = rrscores(expected, ccres, data, @jaccard, @jaccard, ...
                                  @max);
        [ccfmeasure1, ccfmeasure2] = rrscores(expected, ccres, data, @f_measure, @f_measure, @max);
        ccmiscore      = NormalizedMutualInformation(data, expected, ccres);
    else
        ccrel=0; ccrec=0; ccjacc1=0; ccfmeasure1=0; ccmiscore=0;
    end
    plaidres = biclust(data, 'plaid', 'iter_startup', 20, 'iter_layer', 40, 'verbose', false);
    if plaidres.ClusterNo>0
        set(gcf,'visible','off')
        heatmap(data, plaidres, 1);
        print(gcf, '-dpng', [data_type '/' num2str(count) 'plaid.png']);
        close;
        [plaidrel, plaidrec] = rrscores(expected, plaidres, data);
        [plaidjacc1, plaidjacc2]       = rrscores(expected, plaidres, data, @jaccard, ...
                                        @jaccard, @max);
        [plaidfmeasure1, plaidfmeasure2] = rrscores(expected, plaidres, data, @f_measure, @f_measure, @max);
        plaidmiscore      = NormalizedMutualInformation(data, expected, plaidres);
    else
        plaidrel=0; plaidrec=0; plaidjacc1=0; plaidfmeasure1=0; plaidmiscore=0;
    end
    isares = biclust(data, 'isa');
    if isares.ClusterNo>0
        set(gcf,'visible','off')
        heatmap(data, isares, 1);
        print(gcf, '-dpng', [data_type '/' num2str(count) 'isa.png']);
        close;
        [isarel, isarec] = rrscores(expected, isares, data);
        [isajacc1, isajacc2]     = rrscores(expected, isares, data, @jaccard, @jaccard, ...
                                    @max);
        [isafmeasure1, isafmeasure2] = rrscores(expected, isares, data, @f_measure, @f_measure, @max);
        isamiscore      = NormalizedMutualInformation(data, expected, isares);
    else
        isarel=0; isarec=0; isajacc1=0; isafmeasure1=0; isamiscore=0;
    end
    bimaxres = biclust(data, 'bimax', 'numbiclust', nbiclust);
    if bimaxres.ClusterNo>0
        set(gcf,'visible','off')
        heatmap(data, bimaxres, 1);
        print(gcf, '-dpng', [data_type '/' num2str(count) 'bimax.png']);
        close;
        [bimaxrel, bimaxrec] = rrscores(expected, bimaxres, data);
        [bimaxjacc1, bimaxjacc2]       = rrscores(expected, bimaxres, data, @jaccard, ...
                                        @jaccard, @max);
        [bimaxfmeasure1, bimaxfmeasure2] = rrscores(expected, bimaxres, data, @f_measure, @f_measure, @max);
        bimaxmiscore      = NormalizedMutualInformation(data, expected, bimaxres);
    else
        bimaxrel=0; bimaxrec=0; bimaxjacc1=0; bimaxfmeasure1=0; bimaxmiscore=0;
    end
    kspectralres = biclust(data, 'kSpectral');
    if kspectralres.ClusterNo>0
        set(gcf,'visible','off')
        heatmap(data, kspectralres, 1);
        print(gcf, '-dpng', [data_type '/' num2str(count) 'kspectral.png']);
        close;
        [kspectralrel, kspectralrec] = rrscores(expected, kspectralres, data);
        [kspectraljacc1, kspectraljacc2]           = rrscores(expected, kspectralres, data, ...
                                                @jaccard, @jaccard, @max);
        [kspectralfmeasure1, kspectralfmeasure2] = rrscores(expected, kspectralres, data, @f_measure, @f_measure, @max);
        kspectralmiscore      = NormalizedMutualInformation(data, expected, kspectralres);
    else
        kspectralrel=0; kspectralrec=0; kspectraljacc1=0; kspectralfmeasure1=0; kspectralmiscore=0;
    end
%     flocres = biclust(data, 'floc');
%     if flocres.ClusterNo>0
%         set(gcf,'visible','off')
%         heatmap(data, flocres, 1);
%         print(gcf, '-dpng', [data_type '/' num2str(count) 'floc.png']);
%         close;
%         [flocrel, flocrec] = rrscores(expected, flocres, data);
%         [flocjacc1, flocjacc2]      = rrscores(expected, flocres, data, @jaccard, ...
%                                       @jaccard, @max);
%         [flocfmeasure1, flocfmeasure2] = rrscores(expected, flocres, data, @f_measure, @f_measure, @max);
%         flocmiscore      = NormalizedMutualInformation(data, expected, flocres);
%     else
%         flocrel=0; flocrec=0; flocjacc1=0; flocfmeasure1=0; flocmiscore=0;
%     end
    xmotifres = biclust(data, 'xmotif', 'number', nbiclust);
    if xmotifres.ClusterNo>0
        set(gcf,'visible','off')
        heatmap(data, xmotifres, 1);
        print(gcf, '-dpng', [data_type '/' num2str(count) 'xmotif.png']);
        close;
        [xmotifrel, xmotifrec] = rrscores(expected, xmotifres, data);
        [xmotifjacc1, xmotifjacc2]        = rrscores(expected, xmotifres, data, @jaccard, ...
                                          @jaccard, @max);
        [xmotiffmeasure1, xmotiffmeasure2] = rrscores(expected, xmotifres, data, @f_measure, @f_measure, @max);
        xmotifmiscore      = NormalizedMutualInformation(data, expected, xmotifres);
    else
        xmotifrel=0; xmotifrec=0; xmotifjacc1=0; xmotiffmeasure1=0; xmotifmiscore=0;
    end
    if nbiclust==1
        bsgpres = biclust(data, 'bsgp', 'n', (nbiclust+1), 'display', 1);
    else
        bsgpres = biclust(data, 'bsgp', 'n', nbiclust, 'display', 1);
    end
    print(gcf, '-dpng', [data_type '/' num2str(count) 'bsgp.png']); % save spect]rl
    close;
    [bsgprel, bsgprec] = rrscores(expected, bsgpres, data);
    [bsgpjacc1, bsgpjacc2]      = rrscores(expected, bsgpres, data, @jaccard, ...
                                           @jaccard, @max);
    [bsgpfmeasure1, bsgpfmeasure2] = rrscores(expected, bsgpres, data, @f_measure, @f_measure, @max);
    bsgpmiscore      = NormalizedMutualInformation(data, expected, bsgpres);
    lasres = biclust(data, 'las', 'numbc', nbiclust, 'threads', 2);
    if lasres.ClusterNo>0
        set(gcf,'visible','off')
        heatmap(data, lasres, 1);
        print(gcf, '-dpng', [data_type '/' num2str(count) 'las.png']);
        close;
        [lasrel, lasrec] = rrscores(expected, lasres, data);
        [lasjacc1, lasjacc2]     = rrscores(expected, lasres, data, @jaccard, @jaccard, ...
                                    @max);
        [lasfmeasure1, lasfmeasure2] = rrscores(expected, lasres, data, @f_measure, @f_measure, @max);
        lasmiscore = NormalizedMutualInformation(data, expected, lasres);
    else
        lasrel=0; lasrec=0; lasjacc1=0; lasfmeasure1=0; lasmiscore=0;
    end
    if nbiclust==1
        itlres = biclust(data, 'itl', 'numclust', (nbiclust+1), 'display', 1);
    else
        itlres = biclust(data, 'itl', 'numclust', nbiclust, 'display', 1);
    end
    print(gcf, '-dpng', [data_type '/' num2str(count) 'itl.png']);
    close;
    [itlprel, itlprec] = rrscores(expected, itlres, data);
    [itljacc1, itljacc2]       = rrscores(expected, itlres, data, @jaccard, @jaccard, @max);  
    [itlfmeasure1, itlfmeasure2] = rrscores(expected, itlres, data, @f_measure, @f_measure, @max);
    itlmiscore = NormalizedMutualInformation(data, expected, itlres);
    save([data_type '/' num2str(count) '.mat'])
end

function [data, expectedBicluster] = constant_biclust(rowxNum, numxCol, biclusterValue)
% generate constant biclusters
if nargin==2
    biclusterValue = zeros(size(rowxNum,2),1);
end

expectedBicluster = make_expected_biclusters(rowxNum, numxCol);
data = zeros(size(rowxNum,1), size(numxCol,2));
matClust = rowxNum*numxCol;

for num=1:size(rowxNum,2)
    rowClust = find(rowxNum(:,num));
    colClust = find(numxCol(num,:))';
    for i = rowClust(1):rowClust(end)
        for j = colClust(1):colClust(end)
            data(i,j) = data(i,j)+biclusterValue(num);
        end
    end
end
data(find(matClust==0)) = randn(1,length(find(matClust==0)));
end

function [data, expectedBicluster] = shift_scale_data(rowxNum, numxCol, shift, scale)

expectedBicluster = make_expected_biclusters(rowxNum, numxCol);
initialMat = zeros(size(rowxNum,1),size(numxCol,2));
data = initialMat;
numColClust = sum(numxCol,2);
% filling the bicluster values
for num=1:size(rowxNum,2)
    matClust = initialMat;
    parent = randn(1,numColClust(num));
    rowClust = find(rowxNum(:,num));
    colClust = find(numxCol(num,:))';
    matClust(rowClust(1),colClust) = parent;
    if shift==1
        for i = rowClust(2):rowClust(end)
            shiftParameter = randn(1);
            for j = colClust(2):colClust(end)
                matClust(i,j) = matClust(rowClust(1),j) + shiftParameter;
            end
        end
    end
    
    if scale==1
        for i = rowClust(2):rowClust(end)
            scaleParameter = randn(1);
            for j = colClust(2):colClust(end)
                matClust(i,j) = scaleParameter*matClust(rowClust(1),j);
            end
        end
    end
    data = data + matClust;
end
% filling the background
for i = 1:size(data,1)
    for j = find(data(i,:)==0)
        data(i,j) = randn(1);
    end
end
end

function [matrix, expectedBicluster] = plaid_dataset(rowxNum, numxCol, noise)
    
    rowIndicator = rowxNum;
    columnIndicator = numxCol';
    dataMatrixRowSize = size(rowxNum,1);
    dataMatrixColumnSize = size(numxCol,2);
    numBicluster = size(rowxNum,2);
    
    expectedBicluster = make_expected_biclusters(rowIndicator, columnIndicator');
    backgroundEffect = randn(1);
    clusterEffect = randn(numBicluster,1);
    rowEffect = randn(dataMatrixRowSize, numBicluster);
    columnEffect = randn(dataMatrixColumnSize, numBicluster);
    if noise==1
      noise = normrnd(0,0.1,dataMatrixRowSize, dataMatrixColumnSize);
      %noise = randn(dataMatrixRowSize, dataMatrixColumnSize);
        %noise = rand(dataMatrixRowSize, dataMatrixColumnSize);
    else
        noise = zeros(dataMatrixRowSize, dataMatrixColumnSize);
    end


    for i=1:dataMatrixRowSize
        for j=1:dataMatrixColumnSize
            sum = 0;
            for k=1:numBicluster
                sum = sum + (clusterEffect(k,1) + rowEffect(i,k) + columnEffect(j,k))*rowIndicator(i,k)*columnIndicator(j,k);
            end
            matrix(i,j) = backgroundEffect + sum + noise(i,j); 
        end
    end
end
