function rel = relevance(expected, found, datasize)
% Calculates proportion of true negatives to all negatives (specifity)
%
% Author: Jayesh Kumar Gupta, 2013.
%
% Contact: Jayesh Kumar Gupta http://home.iitk.ac.in/~jayeshkg
%          Indian Institute of Technology, Kanpur, India
    
    truenegs = datasize - union_b_area(expected, found);
    allnegs  = datasize - b_area(expected);
    
    if allnegs == 0
      rel = 0;
      return
    end
    rel = truenegs/allnegs;
end 