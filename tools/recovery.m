function area = recovery(expected, found)
% Proportion of true positives
%
% Author: Jayesh Kumar Gupta, 2013.
%
% Contact: Jayesh Kumar Gupta http://home.iitk.ac.in/~jayeshkg
%          Indian Institute of Technology, Kanpur, India

    is_area_nonzero(expected);
    area = intersection_b_area(expected, found) / b_area(expected);
end
